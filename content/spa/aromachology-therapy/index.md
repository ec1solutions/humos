---
title: "Aromachology Services, Treatments and Products."
description: Download Brochure Aromachology examines the relationship and influence of scents on human behaviour, feelings and emotions. It helps uncover which scents have a real impact on you.
heading: "Aromachology Therapy"
subHeading: "Discover the healing power of scents through Aromachology"
layout: pageWithHero
slug: /what-is-aromachology-in-reading/
---

<div class="container py-5">
  <div class="row justify-content-lg-between align-items-lg-center">
    <div class="col-lg-6 mb-3">
      <div class="bg-img-hero-center h-100 min-h-650rem rounded-4" style="background-image: url( images/aromachology-aromatherapy-treatment-reading-2.webp);"></div>
    </div>
    <div class="col-lg-5">
       <h2 class="mb-4">What is Aromachology?</h2>

<p class=""> Aromachology examines the relationship and influence of scents on human behaviour, feelings and emotions. It helps uncover which scents have a real impact on you, what the therapeutic aspect of this impact is and how to use this to improve your well-being.</p>
<p>HUMOS Aromachology also includes all the physical benefits associated with aromatherapy, but without the constraints and limitations of certain oils.</p>
<p>Through Aromachology, HUMOS can develop a bespoke range of therapeutic treatments and products that you respond to both psychologically and physiologically.</p>
<p>This response could be recalling a fond childhood memory or a loved one past, improving the mind for a better night sleep or even improving the overall energy and feeling of openness within your home.</p>
<p class="text-center py-4"><a class="btn btn-lg btn-light rounded-pill py-3 ripple-surface" href="images/aromachology-booklet-web.pdf" target="_blank">Download our brochure</a></p>
</div>
</div>
</div>
<hr>

<div class="container py-5">
  <div class="row justify-content-lg-between align-items-lg-center">
    <div class="col-lg-6 mb-3 order-lg-2">
      <div class="bg-img-hero-center h-100 min-h-650rem rounded-4" style="background-image: url( images/aromachology-aromatherapy-treatment-reading-2.webp);"></div>
    </div>
    <div class="col-lg-5 order-lg-1">
       <h2 class="mb-4">How can Aromachology help you?</h2>
<h6 class=""> Mental</h6>
<p class=""> Life is stressful. HUMOS Aromachology products and treatments factor in your mental well-being, using positive, calming triggers to help keep stress, anxiety and any overwhelming senses in check.</p>
<h6 class=""> Physical</h6>
<p class=""> Skin concern? Physical pain? HUMOS Aromachology tailor-make products and treatments unique to your concerns, increasing their impact and effectiveness.</p>
<h6 class=""> Emotional</h6>
<p class=""> Certain smells may trigger an olfactory flashback or a feeling  or memory unique to you, giving you a sense of positivity which can impact your life and surroundings.</p>
<p class="text-center py-4"><a class="btn btn-lg btn-light rounded-pill py-3 ripple-surface" href="images/aromachology-booklet-web.pdf" target="_blank">Download our brochure</a></p>
</div>
</div>
</div>

<hr>

<div class="container py-5">
  <div class="row justify-content-lg-between align-items-lg-center">
    <div class="col-lg-6 mb-3">
      <img class="img-fluid rounded-4 mb-4" src="images/woman-aromachology-flowers2.webp">
      <img class="img-fluid rounded-4 d-none d-lg-block" src="images/woman-aromachology-flowers2.webp">
    </div>
    <div class="col-lg-5">
       <h2 class="mb-4">Your Aromachology Journey</h2>
<h6>1. Initial consultation</h6>
<p>The foundation for all our Aromachology services and products. This consultation explores your relationship with various odours and scents, uncovering which benefit you physically and mentally. You will also have a greater understanding as to why certain smells impact you the way in which they do.</p>
<h6>2. Trial your sample blends at home</h6>
<p>Upon completion of your consultation, you will be presented with 3 custom blended samples to take home and try. Your aromachologist will provide you with instructions for each blend and what each effects each blend should provide during and after.</p>
<h6>3. Follow-up consultation and blend adjustments</h6>
<p>On your follow-up consultation you and your aromachologist will explore which of the sample blends affected you the most and to what extent. You may discover that more than one blend has had an impact and that different blends have greater impacts at different times throughout the day, with each working in harmony rather than separately.</p>
<p>At this stage your aromachologist may tweak or reformulate your blends based on your experience and feedback.</p>
<h6>4. Product and treatment creation</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor gravida aliquam. Morbi orci urna, iaculis in ligula et, posuere interdum lectus.</p>
<p>Your aromachologist will discuss which products and/or treatments would be a better fit for what you are looking to achieve with your unique blends.</p>
<p class="text-center py-4"><a class="btn btn-lg btn-light rounded-pill py-3 ripple-surface" href="images/aromachology-booklet-web.pdf" target="_blank">Download our brochure</a></p>
</div>

</div>
</div>

<hr>





<div class="container py-5">
<div class="row pb-10">
<div class="col text-center">
<h2 class="">Aromachology products and treatment options</h2>
</div>
</div>


<nav>
<div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
<a class="nav-item nav-link active text-dark" id="nav-home-tab"  data-bs-toggle="tab" data-bs-target="#nav-home" role="tab" aria-controls="home" aria-selected="true"> Therapeutic Fragrance </a>
<a class="nav-item nav-link text-dark" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"> Home Enhancement Products </a>
<a class="nav-item nav-link text-dark" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"> Treatments </a>
<a class="nav-item nav-link text-dark" id="nav-discovery-package-tab" data-bs-toggle="tab" data-bs-target="#nav-discovery-package" role="tab" aria-controls="nav-discovery-package" aria-selected="false"> Discovery Package </a>
</div>
</nav>
<div class="tab-content" id="nav-tabContent">
<div class="tab-pane fade show active py-4" id="nav-home" role="tabpanel">
    <div class="row no-gutters justify-content-lg-between align-items-lg-center">
    <div class="col-12 col-md-6"> <img src="images/humos-aromachology-therapeutic-fragrance-2.webp" class="img-fluid lazyload" alt="">
</div>
<div class="col-12 col-md-5"> <!-- Heading -->
<h3 class=""> Therapeutic Fragrance</h3>
<p>We take perfume back to its therapeutic roots and help you discover the healing power of scents.</p><p>In daily life smells can affect our mood, health, behaviour and emotions and plays both a therapeutic and spiritual role within society.</p>
<p>HUMOS Aromachology Therapeutic Fragrances are bespoke, beneficial olfactory compositions designed for your therapeutic needs, following a thorough aromachology consultation.</p> <!-- Text -->
<ul class="list-group pt-3 pb-5"> 
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">30ml Spray Fragrance</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">30ml Oil Based Fragrance</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">25g Solid Fragrance</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">10ml Pulse Roller</span></li>
</ul>
<button class="btn btn-lg btn-dark rounded-pill py-3 ripple-surface" type="button" data-bs-toggle="modal" data-bs-target="#aromachologyModal">Book your aromachology consultation 
</button>
</div>
</div>
</div>


<div class="tab-pane fade py-4" id="nav-profile" role="tabpanel">
<div class="row no-gutters justify-content-lg-between align-items-lg-center">
<div class="col-12 col-md-6">
<img src="images/aromachology-home-enhancement-products.webp" class="img-fluid lazyload" alt="">
</div>
<div class="col-12 col-md-5"> <!-- Heading -->
<h3 class=""> Aromachology Home Enhancements</h3>
<p>A range of aromatically functional, bespoke products to use as part of your aromachology treatment.</p>
 <!-- Text -->
<ul class="list-group pt-3 pb-5"> 
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Essential Oil (10ml) &#8211; £35</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Essential Oil (20ml) &#8211; £50</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Candle (220g) &#8211; £85</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Bath Oil/Shower (100ml) &#8211; £55</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Reed Diffuser (100ml) &#8211; £60</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Room Spray (100ml) &#8211; £60</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Pillow Mist (30ml) &#8211; £45</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Body Scrub (100g) &#8211; £65</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Bath Salt (100g) &#8211; £65</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Bath Salt (200g) &#8211; £90</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Body Oil (30ml) &#8211; £85</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Body Lotion (100ml) &#8211; £90</span></li>
 </ul> <!-- Button --> 
<button class="btn btn-lg btn-dark rounded-pill py-3 ripple-surface" type="button" data-bs-toggle="modal" data-bs-target="#aromachologyModal">Book your aromachology consultation 
</button>
 </div>
 </div>
 </div>


 <div class="tab-pane fade py-4" id="nav-contact" role="tabpanel">
 <div class="row no-gutters justify-content-lg-between">
 <div class="col-12 col-md-6">
 <img src="images/aromachology-aromathearpy-treatments-in-reading.webp" class="img-fluid lazyload" alt="">
 </div>
 <div class="col-12 col-md-5"> <!-- Heading -->
 <h3 class=""> Aromachology Treatments</h3>
 <p>A selection of aromatically functional, bespoke treatments to use as part of your aromachology treatment.</p>


<div class="accordion mb-4" id="accordionExample"> 
<div class="accordion-item">
                <h2 class="accordion-header" 
                    id="accordion1">
                    <button class="accordion-button collapsed" 
                            type="button" 
                            data-bs-toggle="collapse" 
                            data-bs-target="#accordion1_body" 
                            aria-expanded="false" 
                            aria-controls="collapseOne">
                        <i class="far fa-clock fa-lg fa-fw  me-3"></i>Aromachology Massage Treatment
                    </button>
                </h2>
                <div id="accordion1_body" 
                     class="accordion-collapse collapse" 
                     aria-labelledby="headingOne" 
                     data-bs-parent="#accordionExample">
<div class="accordion-body">
    <h5>Aromachology Massage Treatment</h5>
<p>A multi-sensory massage experience that provides relaxation, restoration and an array of healing benefits that are married with bespoke massage techniques and a combination of aromachology products to help produce your desired results.</p>
 <p>Our aromachology massage utilises various modalities, the use of which will be decided between you and your aromachologist post-consultation:</p>
<ul class="list-group pt-3 pb-5">
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Classic Aromachology with Aromatherapy</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Aromachology candle massage</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Aromachology stone massage (hot and/or cold)</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Aromachology heated oil massage</span></li>
<li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Aromachology heated body butter massage</span></li>
 </ul>
 <p>Each Aromachology Massage Treatment lasts approximately 60 minutes and costs £110.00 per treatment.</p>
                    </div>
</div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" 
                    id="accordion2">
                    <button class="accordion-button collapsed" 
                            type="button" 
                            data-bs-toggle="collapse" 
                            data-bs-target="#accordion2_body" 
                            aria-expanded="false" 
                            aria-controls="collapseOne">
                        <i class="far fa-flask-potion fa-lg fa-fw  me-3"></i>Aromachology Skin Treatment
                    </button>
                </h2>
                <div id="accordion2_body" 
                     class="accordion-collapse collapse" 
                     aria-labelledby="headingOne" 
                     data-bs-parent="#accordionExample">
<div class="accordion-body">
 <h5> Aromachology Skin Treatment</h5>
 <p>A personalised facial therapy that effetctively refines the skin, restoring facial luminescence and creating lasting beauty through bespoke skin-care products which are based on your aromachology consultation.</p>
 <p>These products work in tandem with aromatherapy to help maximise results.</p>
 <ul class="list-group pt-3 pb-5">
<li class="list-group-item d-table"><i class="far fa-clock  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Treatment length: Approximately 60-75 minutes</span></li>
<li class="list-group-item d-table"><i class="far fa-tag  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Cost: £125 per treatment</span></li>
 </ul>
                    </div>
</div>
            </div>
<div class="accordion-item">
                <h2 class="accordion-header" 
                    id="accordion3">
                    <button class="accordion-button collapsed" 
                            type="button" 
                            data-bs-toggle="collapse" 
                            data-bs-target="#accordion3_body" 
                            aria-expanded="false" 
                            aria-controls="collapseOne">
                        <i class="far fa-user-friends fa-lg fa-fw  me-3"></i>Aromachology Body Treatment
                    </button>
                </h2>
                <div id="accordion3_body" 
                     class="accordion-collapse collapse" 
                     aria-labelledby="headingOne" 
                     data-bs-parent="#accordionExample">
<div class="accordion-body">
<h5>Aromachology Body Treatment</h5>
<p>A luxurious full body scrub followed by a deeply rich and nourishing moisturising session that nurtures and guides your skin through a journey of deep-cleansing and detoxification.</p>
 <p>This refiding and restoring body treatment is performed with bespoke body-care based on your aromachology consultation.</p>
 <ul class="list-group pt-3 pb-5">
<li class="list-group-item d-table"><i class="far fa-clock  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Treatment length: Approximately 60-75 minutes</span></li>
<li class="list-group-item d-table"><i class="far fa-tag  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Cost: £120 per treatment</span></li>
 </ul>
                    </div>
                    </div>
                    </div>
                    </div>
<button class="btn btn-lg btn-dark rounded-pill py-3 ripple-surface" type="button" data-bs-toggle="modal" data-bs-target="#aromachologyModal">Book your aromachology consultation 
</button>



</div>
</div>
</div>


 <div class="tab-pane fade py-4" id="nav-discovery-package" role="tabpanel">
 <div class="row no-gutters justify-content-lg-between">
 <div class="col-12 col-md-6">
 <img src="images/aromachology-discovery-package.webp" class="img-fluid lazyload" alt=""></div>
 <div class="col-12 col-md-5"> <!-- Heading -->
 <h3 class=""> Aromachology Discovery Package</h3>
 <p> Discover the world of Aromachology with this exclusive package</p>
 <p> With this discovery package you&#8217;ll explore what oils, scents and smells you positively react to through an introductory consultation where one of our aromachologist will discuss your scent and life experiences, what you would like to achieve from your discovery and what specific smells and scents positively trigger you.</p>
 <p> Your aromachologist will then craft a unique blend for you using what was uncovered through your introductory consultation before formulating this blend in to a body oil which you can take home and use as directed and a pulse roller fragrance to use throughout the day.</p>
 <p> You&#8217;ll then get to experience one of our Aromachology massage treatments in a modality best suited to your needs and concerns that incorporates your unique blend within the massage medium.</p>
 <p> This discovery package includes:
 <ul class="list-group pt-3 pb-5">
 <li class="list-group-item d-table"><i class="far fa-tag  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">Introductory consultation and blending</span></li>
 <li class="list-group-item d-table"><i class="far fa-clock  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">60 minute customised massage incorporating your aromachology blend</span></li>
 <li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">30ml body oil for use at home</span></li>
 <li class="list-group-item d-table"><i class="far fa-flask-potion  fa-lg fa-fw me-3 d-table-cell"></i> <span class="d-table-cell w-100">10ml pulse roller for use at home</span></li>
 </ul>
  This discovery package costs £275 per person and lats between 2.5 and 3 hours.</p> <!-- Button --> 
<button class="btn btn-lg btn-dark rounded-pill py-3 ripple-surface" type="button" data-bs-toggle="modal" data-bs-target="#aromachologyModal">Book your aromachology consultation 
</button>
  </div>
  </div>
  </div>
  </div>
 

 <!--modal -->
<div class="modal fade" id="aromachologyModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="aromachologyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form id="treatmentForm">
                <div class="sending-form modal-body d-none p-3"></div>
                <div class="modal-body form_body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center py-3 px-0">
                            <div class="flex-fill">
                                <input readonly="readonly" type="text" name="WorkshopTitle" class="form-control form-control-lg p-0 m-0 text-capitalize fw-bold border-0 bg-white w-100" value="Book an Aromachology Treatment" />
                            </div>
                        </li>
                    </ul>
                    <div class="border rounded-lg py-2 px-3 mb-3">
                        <div class="d-flex row align-items-center">
                            <div class="col-12">
                            <label for="treatment">Select a treatment:</label>
<select class="form-control mb-3" name="treatment" id="treatment" form="treatmentForm">
    <option value="Select your treatment">Select your treatment</option>
  <option value="Aromachology Massage Treatment">Aromachology Massage Treatment</option>
  <option value="Aromachology Skin Treatment">Aromachology Skin Treatment</option>
  <option value="Aromachology Body Treatment">Aromachology Body Treatment</option>
  <option value="Aromachology Discovery Package">Aromachology Discovery Package</option>
</select>
                            
<div class="mb-3">
                                    <label for="date" class="form-label">Preferred Date</label>
                                    <input type="text" name="date" class="form-control rounded-lg" id="date" data-mdb-input-mask="99/99/9999" data-mdb-mask-placeholder="true" data-mdb-char-placeholder="DD/MM/YYYY" data-mdb-input-placeholder="false" placeholder="e.g. 03/09/2021" required>
                                </div>
                                <div class="mb-3">
                                    <label for="time" class="form-label">Preferred Time</label>
                                    <input type="text" name="time" class="form-control rounded-lg" id="time" data-mdb-input-mask="99:00" data-mdb-mask-placeholder="true" data-mdb-char-placeholder="HH:MM" data-mdb-input-placeholder="false" placeholder="e.g. 18:00" required>
                                </div>
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name</label>
                                    <input type="text" name="name" class="form-control rounded-lg" id="name" placeholder="e.g. Joe Smith" required>
                                </div>
                                <div class="mb-3">
                                    <label for="phone" class="form-label">Phone</label>
                                    <input type="text" name="phone" class="form-control rounded-lg" id="phone" placeholder="e.g. 07747123456" required>
                                </div>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" name="email" class="form-control rounded-lg" id="email" placeholder="e.g. Joe.Smith@domain.com" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center justify-content-between">
                        <button type="submit" class="btn btn-dark">SEND REQUEST</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 
 
  


  

