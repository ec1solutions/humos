---
title: "Delivery and Returns"
description: "Where ever possible, delivery will be made via Royal Mail First Class, unless your order is above a certain value and/or weight, in which case another courier&#8230;"
heading: "Delivery and Returns"
---

<div class="row">
    <div class="col-12">
        <h3>Delivery:</h3>
    </div>
    <div class="col-12 mb-md-5 mb-5">
        <p>Where ever possible, delivery will be made via Royal Mail First Class, unless your order is above a certain value and/or weight, in which case another courier will be used. For deliveries outside of the U.K., you will be responsible for any import duties or VAT due at the point of delivery/import to your destination country.</p>
        <p>We aim to dispatch orders within 48-72 hours of your order being received.</p>
    </div>
    <div class="col-12">
        <h3>Returns:</h3>
    </div>
    <div class="col-12">
        <p>If you need to return all or part of your order, please contact our <a href="/contact-us/" class="fw-bold btn-link text-dark">customer services team</a> for a returns authorisation number. Please remember that returns can only be accepted in accordance with our <a href="/terms-and-conditions/" class="fw-bold btn-link text-dark">terms and conditions</a> and must be in their original unused, unopened state. Customised or bespoke products cannot be returned unless faulty.</p>
    </div>
</div>
