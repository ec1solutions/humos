---
title: "Nominal Amount Gift Card"
description: "Nominal Amount Gift Card"
product_id: "NOMINAL_AMOUNT_GIFT_CARD"
layout: "gift-cards-nominal"
image: "/images/products/essential-oil-blends/fiery-romance-essential-oil-blend.jpg"
price: "10.00"
select1_value: "10.00"
select1_text: "£10.00"
select2_value: "50"
select2_text: "£50.00"
select3_value: "100"
select3_text: "£100.00"
select4_value: "500"
select4_text: "£500"
price_variables: "10[+00.00]|50[+40.00]|100[+90.00]|500[+490.00]"
---
I've guessed at some figures.