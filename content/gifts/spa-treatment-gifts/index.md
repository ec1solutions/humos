---
title: "Spa Treatment Gift Card"
description: "Spa Treatment  Gift Card"
product_id: "SPA_TREATMENT_GIFT_CARD"
layout: "gift-cards-with-select"
image: "/images/products/essential-oil-blends/fiery-romance-essential-oil-blend.jpg"
price: "65"
select1_value: "Aromachology Massage Treatment"
select1_text: "Aromachology Massage Treatment (£110.00)"
select2_value: "Aromachology Skin Treatment"
select2_text: "Aromachology Skin Treatment (£125.00)"
select3_value: "Aromachology Body Treatment"
select3_text: "Aromachology Body Treatment (£120.00)"
select4_value: "Discovery Package Aromachology Discovery Package"
select4_text: "Discovery Package Aromachology Discovery Package (£275.00)"
select5_value: "Custom Head-to-Toe Aromatherapy Massage"
select5_text: "Custom Head-to-Toe Aromatherapy Massage (£175.00)"
select6_value: "Custom Body Polish Package"
select6_text: "Custom Body Polish Package (£125.00)"
select7_value: "Salt and Oil Aromatherapy Scrub"
select7_text: "Salt and Oil Aromatherapy Scrub (£65.00)"
select8_value:  "Bespoke Aromatherapy Facial with Customised Facial Massage"
select8_text: "Bespoke Aromatherapy Facial with Customised Facial Massage (£165.00)"
price_variables: "Aromachology Massage Treatment[+45.00]|Aromachology Skin Treatment[+60.00]|Aromachology Body Treatment[+55.00]|Aromachology Discovery Package[+210.00]|Custom Head-to-Toe Aromatherapy Massage[+110.00]|Custom Body Polish Package[+60.00]|Salt and Oil Aromatherapy Scrub[+0.00]|Bespoke Aromatherapy Facial with Customised Facial Massage[+100.00]"
---
SpaLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Auctor eu augue ut lectus arcu bibendum.