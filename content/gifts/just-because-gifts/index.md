---
title: "Just Because Gift Card"
description: "Just Because Gift Card"
product_id: "JUST_BECAUSE_GIFT_CARD"
layout: "gift-cards-with-select"
image: "/images/products/essential-oil-blends/fiery-romance-essential-oil-blend.jpg"
price: "29.95"
select1_value: "Reed Diffusers"
select1_text: "Reed Diffusers (£29.95)"
select2_value: "Home Spray / Room Fragrance"
select2_text: "Home Spray / Room Fragrance (£49.95)"
select3_value: "Candles"
select3_text: "Candles (£39.95)"
select4_value: "Blended Oils"
select4_text: "Blended Oils (£49.95)"
price_variables: "Reed Diffusers[+00.00]|Home Spray / Room Fragrance[+20.00]|Candles[+10.00]|Blended Oils[+20.00]"
---
I do not know what this page relates to.