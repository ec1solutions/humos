---
title: "Fragrance Design & Creation Gift Card"
description: "Fragrance Design & Creation Gift Card"
product_id: "FRAGRANCE_DESIGN_AND_CREATION_GIFT_CARD"
layout: "gift-cards-with-select"
image: "/images/products/essential-oil-blends/fiery-romance-essential-oil-blend.jpg"
price: "100"
select1_value: "Fragrance Making Experience"
select1_text: "Fragrance Making Experience (£165.00)"
select2_value: "Open Fragrance Workshop"
select2_text: "Open Fragrance Workshop (£150.00)"
select3_value: "Private Group Fragrance Workshop"
select3_text: "Private Group Fragrance Workshop (£595.00)"
select4_value: "Reed Diffuser & Home Spray Making"
select4_text: "Reed Diffuser & Home Spray Making (£165.00)"
select5_value: "Open Home Fragrance Workshop"
select5_text: "Open Home Fragrance Workshop (£100.00)"
select6_value: "Private Group Home Fragrance Making Workshop"
select6_text: "Private Group Home Fragrance Making Workshop (£425.00)"
select7_value: "Open Candle Making Workshop"
select7_text: "Open Candle Making Workshop (£100.00)"
select8_value: "Private Group candle making Workshop"
select8_text: "Private Group candle making Workshop (£590.00)"
price_variables: "Fragrance Making Experience[+65.00]|Open Fragrance Workshop[+50.00]|Private Group Fragrance Workshop[+495.00]|Reed Diffuser & Home Spray Making[+65.00]|Open Home Fragrance Workshop[+0.00]|Private Group Home Fragrance Making Workshop[+325.00]|Open Candle Making Workshop[+0.00]|Private Group candle making Workshop[+490.00]"
---
ssLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nec feugiat in fermentum posuere urna nec tincidunt praesent semper. Auctor eu augue ut lectus arcu bibendum.