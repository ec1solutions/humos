---
title: "Re-order your custom fragrance"
description: "Re-order a fragrance"
product_id: "RE-ORDER_A_FRAGRANCE"
layout: "product-re-order"
image: "/images/products/essential-oil-blends/fiery-romance-essential-oil-blend.jpg"
price: "120"
---
Re-order your creation. Simply provide your formulation code and we’ll get to work on blending for you. You’ll find your formulation code printed on the label attached to the bottle of your perfume.

If the label is damaged, please <a href="/contact-us/">contact us</a> and we’ll do our best to locate your formulation code.